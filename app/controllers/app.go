package controllers

import (
	"encoding/json"
	"github.com/robfig/revel"
	"net/http"
	//"fmt"
	//"github.com/ziutek/mymysql/mysql"
    //_ "github.com/ziutek/mymysql/native"
	
	"forum/app/models"
	//"os"
	"time"
)


type Cookie struct {
		Name  string
		Value  string
		Path  string
}

type Application struct {
	*revel.Controller
}

func Json_encode(data interface{}) string {
	b, err := json.Marshal(data)

	if err != nil {
		return "{}"
	}

	return string(b)
}

func (c Application) Set_cookie(name string, value string) {
	c.SetCookie(&http.Cookie{
		Name:  name,
		Value: value,
		Path:  "/",
		Expires: time.Now().AddDate(0, 3, 0) ,
	})
}


func (c Application) Index(w http.ResponseWrite, r *http.Request) revel.Result {

	cookies := r.Cookies(r) 
	for _, cookie := range cookies {
		//if err != nil {
			return c.RenderText(cookie.Value)
		
	}
	//return c.RenderJson(c.Cookie("token"))

	return c.Render()
}

func (c Application) Sections(s models.Section) revel.Result {

	return c.RenderText(Json_encode(s.GetAll()))

}

func (c Application) Threads(id int, page int, t models.Thread) revel.Result {
	type result struct {
		Threads models.Threads  `json:"threads"`
		Info    models.Info 	`json:"info"`
		Pages   int 			`json:"pages"`
	}

	threads, info, pages := t.GetId(id, page)

	return c.RenderJson(result{
			Threads: threads,
			Info: info,
			Pages: pages,
		})

}

func (c Application) Answers(id int, page int, a models.Answer) revel.Result {
	type result struct {
		Answers models.Answers  `json:"answers"`
		Info    models.Info 	`json:"info"`
		Pages   int 			`json:"pages"`
	}

	answers, info, pages := a.GetAll(id, page)

	return c.RenderJson(result{
			Answers: answers,
			Info: info,
			Pages: pages,
		})

}

func (c Application) Auth(email string, password string, remember string, u models.User) revel.Result {
	
	c.Set_cookie("token", u.NewToken(email, password))

	return c.RenderJson(u.Chech_pass(email, password))
}