package models

import (
	//"reflect"
	//"encoding/json"
	//"github.com/robfig/revel"
	//"github.com/ziutek/mymysql/mysql"
   // _ "github.com/ziutek/mymysql/native"
    
    "forum/app/plugins"
    //"runtime"
)


type Thread struct {
    Answers         int    `json:"answers"`
    Name            string `json:"name"`
    Id              int    `json:"id"`
    Author          int    `json:"author"`
    Attached        int    `json:"attached"`
    Section         int    `json:"section"`
    Rating          int    `json:"rating"`    
    Views           int    `json:"views"`
    Updated_at      int    `json:"updated_at"`
    Created_at      int    `json:"created_at"`
    Closed          int    `json:"closed"`
    Username        string `json:"username"`
    Last            int    `json:"last"`
}

type Threads map[string]Thread

type Info struct {
    Count int `json:"count"`
    Name string `json:"name"`
    Id int `json:"id"`
}



func (s *Thread) GetId(id int, page int) (Threads, Info, int) {
      	rows:= 20
      	if page < 1 {
      		page = 1
      	}

        start:= page * rows - rows
        stop:= page * rows

        i := 0

        list := make(Threads)




        query:= "SELECT count(DISTINCT a.id) AS answers, t.name, t.id, t.author, t.attached, t.section, t.rating, t.views, t.updated_at, t.created_at,  t.closed,  u.login AS username, MAX(a.created_at) AS last FROM threads AS t  RIGHT JOIN answers AS a ON t.id = a.thread LEFT JOIN users AS u ON u.id = t.author WHERE t.section = ? GROUP BY t.id ORDER BY IF(t.attached = 1, 9999999999 , MAX(a.created_at)) DESC LIMIT ?,?"

//id  start   stop

        stmt, err := db.Con.Prepare(query)

        th, res, err := stmt.Exec(id, start, stop)
        //threads 
    	if err != nil {
        	panic(err)
    	}

        for _, row := range th {
            list[string(i)] = Thread{
                Answers:     row.Int(res.Map("answers")),
                Name:        row.Str(res.Map("name")),
                Id:          row.Int(res.Map("id")),
                Author:      row.Int(res.Map("author")),
                Attached:    row.Int(res.Map("attached")),
                Section:     row.Int(res.Map("section")),
                Rating:      row.Int(res.Map("rating")),
                Views:       row.Int(res.Map("views")),
                Updated_at:  row.Int(res.Map("updated_at")),
                Created_at:  row.Int(res.Map("created_at")),
                Closed:      row.Int(res.Map("closed")),
                Username:    row.Str(res.Map("username")),
                Last:        row.Int(res.Map("last")),   
            }

            i++
        }


        stmt2, err:= db.Con.Prepare("SELECT COUNT(s.id) as count , s.name, s.id FROM sections as s RIGHT JOIN threads as t ON s.id = t.section WHERE s.id = ?")
        inf, res2, err := stmt2.Exec(id)

        if err != nil {
            panic(err)
        }

        var info Info

        for _, row := range inf {
            info = Info{
                Count: row.Int(res2.Map("count")),
                Name: row.Str(res2.Map("name")),
                Id: row.Int(res2.Map("id")),
            }
        }

        
        pages := info.Count / rows

        //$this->response(array(
        //    'info' => $info,
        //    'pages' => $pages,
        //    'threads' => $threads
        //));
		return list, info, pages
}
