package models

import (
	//"reflect"
	//"encoding/json"
	//"github.com/robfig/revel"
	//"github.com/ziutek/mymysql/mysql"
   // _ "github.com/ziutek/mymysql/native"
    
    "forum/app/plugins"
    //"runtime"
)

/* in model.threads
type Info struct {
    Count int `json:"count"`
    Name string `json:"name"`
    Id int `json:"id"`
}*/


type Answer struct{
    Id          int     `json:"id"`
    Message     string  `json:"message"`
    Author      int     `json:"author"`
    Rating      int     `json:"rating"`
    Name        string  `json:"name"`
    Views       int     `json:"views"`
    Attached    int     `json:"attached"`
    Closed      int     `json:"closed"`
    Updated_at  int     `json:"updated_at"`
    Created_at  int     `json:"created_at"`
    Section     int     `json:"section"`
    Username    string  `json:"username"`  
    Avatar      string  `json:"avatar"`
 }

type Answers map[string]Answer


func (a *Answer) GetAll(id int, page int) (Answers, Info, int) {
        rows:= 20;
        if page < 1 {
            page = 1
        }

        start:= page * rows - rows
        stop:= page * rows

        i := 0

        list := make(Answers)


        query:= "SELECT `a`.id, `a`.message, `a`.author, `u`.rating, `t`.name, `t`.views,`t`.attached, `t`.closed, `a`.created_at, `a`.updated_at, `t`.section, `u`.login as `username`, `u`.avatar FROM `threads` AS `t` RIGHT JOIN `answers` AS `a` ON `a`.`thread` = `t`.`id` LEFT JOIN `users` AS `u` ON `u`.`id` = `a`.`author` WHERE `t`.`id` = ? LIMIT ?,?"
        stmt, err := db.Con.Prepare(query)

        answ, res, err := stmt.Exec(id, start, stop)
        //threads 
        if err != nil {
            panic(err)
        }

        for _, row := range answ {
            list[string(i)] = Answer{
                Id:          row.Int(res.Map("id")),
                Message:     row.Str(res.Map("message")),
                Author:      row.Int(res.Map("author")),
                Rating:      row.Int(res.Map("rating")),
                Name:        row.Str(res.Map("name")),
                Views:       row.Int(res.Map("views")),
                Attached:    row.Int(res.Map("attached")),
                Closed:      row.Int(res.Map("closed")),
                Updated_at:  row.Int(res.Map("updated_at")),
                Created_at:  row.Int(res.Map("created_at")),
                Section:     row.Int(res.Map("section")),
                Username:    row.Str(res.Map("username")),   
                Avatar:      row.Str(res.Map("avatar")), 
            }

            i++
        }

        //$query = 'UPDATE `threads` SET `views` = IFNULL(`views` + 1, 1) WHERE `id` = '. $thread;
        //DB::query($query, DB::UPDATE)->execute();


        stmt2, err:= db.Con.Prepare("SELECT COUNT(`a`.`id`) as `count` , `t`.`closed`, `t`.`name`, `t`.`id`, `s`.`name` as `section_name`, `s`.`id` as `section_id`,`t`.`attached` FROM `threads` as `t` RIGHT JOIN `sections` as `s` ON `s`.`id` = `t`.`section` RIGHT JOIN `answers` as `a` ON `t`.`id` = `a`.`thread` WHERE `t`.`id` = ?")
        inf, res2, err := stmt2.Exec(id)

        if err != nil {
            panic(err)
        }

        var info Info

        for _, row := range inf {
            info = Info{
                Count: row.Int(res2.Map("count")),
                Name: row.Str(res2.Map("name")),
                Id: row.Int(res2.Map("id")),
            }
        }

        
        pages := info.Count / rows

        //$this->response(array(
        //    'info' => $info,
        //    'pages' => $pages,
        //    'threads' => $threads
        //));
        return list, info, pages

}