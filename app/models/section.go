package models

import (
	//"reflect"
	//"encoding/json"
	//"github.com/robfig/revel"
	//"github.com/ziutek/mymysql/mysql"
   // _ "github.com/ziutek/mymysql/native"
    
    "forum/app/plugins"
    //"runtime"
)


type Section struct {
	Count       int    `json:"count"`
	Answers     int    `json:"answers"`
	Name        string `json:"name"`
	Id          int    `json:"id"`
	Parent      int    `json:"parent"`
	Icon        string `json:"icon"`
	Last        int    `json:"last"`
	User        int    `json:"user"`	
	Description string `json:"description"`

}

type Sections map[string]Section

func (s *Section) GetAll() Sections {



	rows, res, err := db.Con.Query("SELECT count(DISTINCT `t`.`id`) AS `count`, count(DISTINCT `a`.`id`) AS `answers`, `s`.`name`,`s`.`id`,`s`.`parent`,`s`.`description`,`s`.`icon`,MAX(`a`.`created_at`) AS `last`,MAX(CASE WHEN `a`.`created_at` THEN `a`.`author` ELSE 0 END) AS `user`  FROM `threads` AS `t` RIGHT JOIN `answers` AS `a` ON `t`.`id` = `a`.`thread` RIGHT JOIN `sections` AS `s` ON `s`.`id` = `t`.`section` GROUP BY `s`.`id` ORDER BY `s`.`sort` ASC")
    
    if err != nil {
        panic(err)
    }

	i := 0

		list := make(Sections)


	for _, row := range rows {

		list[string(i)] = Section{
			Count:       row.Int(res.Map("count")),
			Answers:     row.Int(res.Map("answers")),
			Id:          row.Int(res.Map("id")),
			Name:        row.Str(res.Map("name")),
			Parent:      row.Int(res.Map("parent")),
			Icon:        row.Str(res.Map("icon")),
			Last:        row.Int(res.Map("last")),
			User:        row.Int(res.Map("user")),
			Description: row.Str(res.Map("description")),
			
		}

        i++
    }

	return list
}
