package models

import (
	//"reflect"
	//"encoding/json"
	//"github.com/robfig/revel"
	//"github.com/ziutek/mymysql/mysql"
   // _ "github.com/ziutek/mymysql/native"
    "crypto/md5"
    "crypto/sha1"
    "forum/app/plugins"
    "io"
    "math/rand"
    "time"
    "strings"
    "fmt"
)

//revel.INFO.Println("Running revel server")
type User struct { 
    Id  int    `json:"id"`
    Login  string    `json:"login"`
    Name  string    `json:"name"`
    Family  string    `json:"family"`
    Answers  int    `json:"answers"`
    Rating  int    `json:"rating"`
    Plus  int    `json:"plus"`
    Minus  int    `json:"minus"`
    Info  string    `json:"info"`
    Sign  int    `json:"sign"`
    Birthday  int    `json:"birthday"`
    Email  string    `json:"email"`
    Site  string    `json:"site"`
    Icq  string    `json:"icq"`
    Themes  int    `json:"themes"`
    Last_msg  int    `json:"last_msg"`
    U_schemes  int    `json:"u_schemes"`
    U_companents  int    `json:"u_companents"`
    P_schemes  int    `json:"p_schemes"`
    P_companents  int    `json:"p_companents"`
    Reg_blog  int    `json:"reg_blog"`
    Ip_auth  int    `json:"ip_auth"`
    Status  int    `json:"status"`
    Ban  int    `json:"ban"`
    Bans  int    `json:"bans"`
    Avatar  string    `json:"avatar"`
    Jabber  string    `json:"jabber"`
    P_email  int    `json:"p_email"`
    Gold  int    `json:"gold"`
    password  string
    Token  string    `json:"token"`
    Token_time  int    `json:"token_time"`
    Location  string    `json:"location"`
    City  string    `json:"city"`
    Country  string    `json:"country"`
    Language  string    `json:"language"`
    Style  string    `json:"style"`
    Created_at  int    `json:"created_at"`
    Updated_at  int    `json:"updated_at"`
}


func randInt(min int, max int) string {
    rand.Seed(time.Now().UTC().UnixNano())
    return fmt.Sprintf("%d", min + rand.Intn(max-min))
}

func crypt_md5(str string) string {
    h := md5.New()
    io.WriteString(h, str)
    return fmt.Sprintf("%x", h.Sum(nil))
}

func crypt_sha1(str string) string {
    h := sha1.New()
    io.WriteString(h, str)
    return fmt.Sprintf("%x", h.Sum(nil))
}



func str_replace(str1 string, str2 string, subject string) string {
    return strings.Replace(subject, str1, str2, -1)
}

func (u *User) NewToken(email string, hash string) string {
    token := crypt_md5(email + crypt_sha1(crypt_md5(hash+"!3D2sd4$%\"\\08"+email)))

    token = str_replace("f", "8", token)
    token = str_replace("4", "5", token)
    token = str_replace("a", "9", token)
    token = str_replace("c", "0", token)

    str := hash + randInt(1111111, 9999999) + crypt_md5(token) + randInt(1111111, 9999999)

    return crypt_md5(str)
}
func (u *User) Crypt_pass(email string, password string) string {
    token := crypt_md5(email + crypt_sha1(crypt_md5(password+"!3D2sd4$%\"\\08"+email)))

    token = str_replace("a", "0", token)
    token = str_replace("8", "c", token)
    token = str_replace("f", "6", token)
    token = str_replace("4", "7", token)

    return crypt_md5(token)
}



func (u *User) Chech_pass(email string, hash string) User {
        query:= "SELECT * FROM users WHERE email = ? AND password = ? LIMIT 1"

        stmt, err := db.Con.Prepare(query)

        rows, res, err := stmt.Exec(email, u.Crypt_pass(email, hash))

        if err != nil {
            panic(err)
        }

        for _, row := range rows {
            return  User{
                Id: row.Int(res.Map("id")),
                Login: row.Str(res.Map("login")),
                Name: row.Str(res.Map("name")),
                Family: row.Str(res.Map("family")),
                Answers: row.Int(res.Map("answers")),
                Rating: row.Int(res.Map("rating")),
                Plus: row.Int(res.Map("plus")),
                Minus: row.Int(res.Map("minus")),
                Info: row.Str(res.Map("info")),
                Sign: row.Int(res.Map("sign")),
                Birthday: row.Int(res.Map("birthday")),
                Email: row.Str(res.Map("email")),
                Site: row.Str(res.Map("site")),
                Icq: row.Str(res.Map("icq")),
                Themes: row.Int(res.Map("themes")),
                Last_msg: row.Int(res.Map("last_msg")),
                U_schemes: row.Int(res.Map("u_schemes")),
                U_companents: row.Int(res.Map("u_companents")),
                P_schemes: row.Int(res.Map("p_schemes")),
                P_companents: row.Int(res.Map("p_companents")),
                Reg_blog: row.Int(res.Map("reg_blog")),
                Ip_auth: row.Int(res.Map("ip_auth")),
                Status: row.Int(res.Map("status")),
                Ban: row.Int(res.Map("ban")),
                Bans: row.Int(res.Map("bans")),
                Avatar: row.Str(res.Map("avatar")),
                Jabber: row.Str(res.Map("jabber")),
                P_email: row.Int(res.Map("p_email")),
                Gold: row.Int(res.Map("gold")),
                password: row.Str(res.Map("password")),
                Token: row.Str(res.Map("token")),
                Token_time: row.Int(res.Map("token_time")),
                Location: row.Str(res.Map("location")),
                City: row.Str(res.Map("city")),
                Country: row.Str(res.Map("country")),
                Language: row.Str(res.Map("language")),
                Style: row.Str(res.Map("style")),
                Created_at: row.Int(res.Map("created_at")),
                Updated_at: row.Int(res.Map("updated_at")),
            }
        }
        return User{}
}

//func auth(password, email) {
        /*$hash = Model_User::crypt_pass($email, $password);
        $user = Model_User::chech_pass($hash, $email);
        if($user) {
            $token = Model_User::new_token($email, $hash);
            $user->set('token', $token);
            $user->save();
            //setcookie("token", $token, time() + 2592000);
            Cookie::set('token', $token, 2592000);

            $user = $user->to_Array();
            unset($user['password']);

            return $user;
        }*/
//}