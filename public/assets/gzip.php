<?php

	$allow_ext = 'css,js';
	$gz_ext = 'gz';
	const COMPRESS_LEVEL = 7;

	//костыль для win, сканирует +2 уровня вложености
	foreach(glob("*/{*/*/*,*/*,*}.{{$allow_ext}}", GLOB_BRACE) as $f) {
		$data = file_get_contents($f);
		$data = gzencode($data, COMPRESS_LEVEL);
		file_put_contents("$f.$gz_ext", $data);
	}

?>