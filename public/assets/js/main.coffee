@app = angular.module('app', ['ngResource'], ($routeProvider, $locationProvider, $interpolateProvider) ->
  $locationProvider.hashPrefix('!')
   .html5Mode(false);

  $interpolateProvider.startSymbol('[{')
  $interpolateProvider.endSymbol('}]')

  $routeProvider.when('/services', {
    templateUrl: 'services.html'
    controller: 'SectionsCtrl'
  })
  .when('/forum/section/:id/page/:page', {
    templateUrl: 'threads.html'
    controller: 'ThreadsCtrl'
  })
  .when('/forum/thread/new', {
    templateUrl: 'newThread.html'
    controller: 'AddThreadPageCtrl'
  })
  .when('/forum/section/:sid/thread/:tid/page/:page', {
    templateUrl: 'answers.html'
    controller: 'AnswersCtrl'
  })
  .when('/forum/last', {
    templateUrl: 'threads.html'
    controller: 'LastCtrl'
  })
  .when('/forum', {
    templateUrl: 'sections.html'
    controller: 'SectionsCtrl'
  })
  .when('/dev', {
    templateUrl: 'dev.html'
    controller: 'DevCtrl'
  })
  .when('/forum/registration', {
    templateUrl: 'reg.html'
    controller: 'UserCtrl'
  })
  .when('/forum/sha', {
    templateUrl: 'sha.html'
    controller: 'UserCtrl'
  })
  .when('/forum/profile/:username', {
    templateUrl: 'profile.html'
    controller: 'ProfileCtrl'
  })
  .when('/forum/profile/:username/edit', {
    templateUrl: 'profile_edit.html'
    controller: 'ProfileCtrl'
  })

  $routeProvider.otherwise({redirectTo: '/forum'});
)
 .directive('uiBreadcrumb', ->
    return {
      restrict: 'E'
      template: "<ul class=\"breadcrumb\" style=\"margin: 0px\"><li ng-repeat='node in breadcrumb' ng-class=\"{active: $last}\" ><span ng-show=\"$last\">{{node.label}}</span><a ng-hide=\"$last\" ng-href='{{node.url}}'><i ng-show=\"$first\" class=\"icon icon-home\"></i> {{node.label}}</a> <span ng-hide=\"$last\" class=\"divider\">/</span></li></ul>"
      replace: true
    }
)
 .directive('autoComplete', ($compile)->
      link: ($scope, elem, attr, ctrl)->
        console.debug $scope
)
.directive('uiPagination', ->
    object =
      restrict: 'EA'
      replace: true
      template:
        '<div class="pagination pagination-centered" style="margin: 0px;height: 29px">' +
        '<ul>' +
          '<li ng-class="{disabled: firstPage()}" ng-click="goToFirstPage()">' +
            '<a><i class="icon-step-backward"></i></a>' +
          '</li>' +
          '<li ng-class="{disabled: !hasPrev()}" ng-click="prev()">' +
            '<a><i class="icon-chevron-left"></i></a>' +
          '</li>' +
          '<li ng-repeat="page in pages"' +
          'ng-class="{active: isCurrent(page)}"' +
          'ng-click="setCurrent(page)"' +
          '>' +
            '<a>{{page}}</a>' +
          '</li>' +
          '<li ng-class="{disabled: !hasNext()}" ng-click="next()">' +
            '<a><i class="icon-chevron-right"></i></a>' +
          '</li>' +
          '<li ng-class="{disabled: lastPage()}" ng-click="goToLastPage()">' +
            '<a><i class="icon-step-forward"></i></a>' +
          '</li>' +
        '</ul>' +
        '</div>'
      scope:
        cur: '='
        total: '='
        display: '@'

      link: (scope, element, attrs)->
          calcPages = ->
              display = +scope.display
              delta = Math.floor(display / 2)
              scope.start = scope.cur - delta
              if scope.start < 1
                  scope.start = 1
              scope.end = scope.start + display - 1
              if scope.end > scope.total
                  scope.end = scope.total
                  scope.start = scope.end - (display - 1);
                  if scope.start < 1
                      scope.start = 1

              start = scope.start
              end = scope.end
              scope.pages = [start..end]
          scope.$watch('cur', calcPages)
          scope.$watch('total', calcPages)
          scope.$watch('display', calcPages)

          scope.isCurrent = (index)->
              return scope.cur*1 == index*1
          scope.setCurrent = (index) ->
              scope.cur = index
          scope.hasPrev = ->
              return scope.cur > 1
          scope.prev = ->
              if scope.hasPrev()
                  scope.cur--
          scope.hasNext = ->
              return scope.cur < scope.total
          scope.next = ->
              if scope.hasNext()
                  scope.cur++
          scope.firstPage = ->
              return scope.start*1 == 1
          scope.goToFirstPage = ->
               if !scope.firstPage()
                    scope.cur = 1
          scope.lastPage = ->
              return scope.end*1 == scope.total*1
          scope.goToLastPage = ->
            if !scope.lastPage()
                scope.cur = scope.total
)
.factory('Sections', ($rootScope, $http, $q, $routeParams, $timeout, $resource, $cacheFactory) ->
  cache = $cacheFactory('cacheSections')
  Sections = $resource('/forum/sections/', {} ,{get: { method: 'GET', isArray : true }})

  return {
    get: (callback)->
      sections = cache.get(1);
      if !sections
        sections = Sections.get(callback);
        cache.put(1, sections);
      else
        if callback
          callback(sections)

      return sections;
  }
)
.factory('User', ($rootScope, $http, $q, $routeParams, $timeout, $resource, $cacheFactory)->
    cache = $cacheFactory('cacheUser')
    User = $resource('/forum/user/:username', $routeParams ,{get: { method: 'GET', isArray : false }})

    return {
        get: (username)->
            username = username || $routeParams.username

            user = cache.get(username);
            if !user
               user = User.get({username: username})
               cache.put(username, user);

            return user;
    }
)
.factory('City', ($http, $q, $timeout, $resource, $cacheFactory)->
    cache_city = $cacheFactory('cacheCity')
    return {
        get: (callback)->
          City = $resource('/assets/json/city_ru.txt', {}, {get: {isArray: true, method: 'GET'}  })
          city_ru = cache_city.get('city_ru')
          if !city_ru
            city_ru = City.get(callback)
            cache_city.put('city_ru', city_ru)

          if callback
            callback(cache_city)
          return city_ru
    }
)
.factory('Threads', ($routeParams, $http, $q, $timeout, $resource, $cacheFactory) ->
    cache = $cacheFactory('cacheSection');
    return {
        get: (callback)->
            Threads = $resource('/forum/section/:id/page/:page', $routeParams)
            section = Threads.get(callback)
            return section
        getLast: (callback)->
            ThreadsLast = $resource('/forum/last')
            return ThreadsLast.get(callback)
    }
)
.factory('Thread', (Threads, $rootScope, $routeParams, $http, $q, $timeout, $resource, $cacheFactory)->
    return {
        save: (p, t)->
          if $rootScope.user.status > 1
              if t == 'attached'
                  if !p.attached
                      success("Тема успешно откреплена", 3)
                  else
                      success("Тема успешно закреплена", 3)
              if t == 'closed'
                  if p.closed
                      success("Тема успешно открыта", 3)
                  else
                      success("Тема успешно закрыта", 3)
              if t == 'name'
                  success('Тема успешно переименована', 3)
              if t == 'move'
                  success('Тема успешно перемещена', 3)
              if t == 'del'
                  success('Тема успешно удалена', 3)
          else
            error('Доступ запрещён!', 3)
          #angularjs fix
          for i in p
            p[i] = p[i] == null ? '' : p[i]
          return $resource('/forum/thread/:id/page/1', p ,{save: { method: 'PUT'}}).save()
    }
)

.run( (Threads, $q, $routeParams, $rootScope, Sections, $http, $resource) ->
    $rootScope.activeTab = '';
    $rootScope.user = CONFIG.user

    $rootScope.$on('$routeChangeStart', ($scope, next)->
        $scope.section_id = next.params.id || '' + next.params.sid || ''
    )


    $rootScope.rating = (rating, i) ->
        if rating == 'start'
          return [1,2,3,4,5];
        if i < rating
          return 'star__on.png';
        else
          return 'star__off.png';

    $rootScope.safeApply = (fn)->
        phase = this.$root.$$phase;
        if phase == '$apply' || phase == '$digest'
            if fn && (typeof(fn) == 'function')
                fn()
        else
          this.$apply(fn)




)
.controller('NavCtrl', ( $rootScope, $scope, $location, $routeParams) ->

    $scope.subColors = ['DE662B', '21CFDC', '1F901B', 'EDDE45', '5882B8'];
    $scope.navClass =  (page, menu, key)->
        page = page.substring(2)
        if $location.path() == page || $location.path().indexOf(page.substring(2)) != -1
            _class = 'active'
            $rootScope.activeTab = key
        else
            _class = ''

        return _class

    $scope.getSubMenu = ->
        if $scope.items[$rootScope.activeTab]
          return $scope.items[$rootScope.activeTab].menu

    $scope.isThread = ->
        return $routeParams.tid

    $scope.items =
      [
        {
          name:'Форум'
          href: '#!/forum'
          menu: [
            {name: 'Последние', 'href': '#!/forum/last'}
            {name: 'Лог разработки', 'href': '#!/dev'}
            {name: 'Прочесть все', 'href': '#!/forum'}
            {name: 'Новая тема', 'href': '#!/forum/thread/new'}
          ]
        }
        {
          name: 'Сервисы'
          href: '#!/services'
          menu: []
        }
        {
          name: 'Пользователи'
          href: '#!/services'
          menu: [
            {name: 'Оценки', 'href': '#!/forum'}
            {name: 'Баны', 'href': '#!/forum'}
            {name: 'Статистика', 'href': '#!/forum'}
          ]
        }
      ]
)
.factory('Answers', ($routeParams, $http, $q, $timeout, $resource)->
    return $resource('/forum/thread/:tid/page/:page', $routeParams)
)
.controller('UserCtrl', ($rootScope, $scope, $http, $location,$timeout) ->
    $scope.exit = ->
        $.removeCookie('token', {path: '/forum'})
        $rootScope.user = {};

    $scope.fields = { }

    $scope.animate = (a, text_rating, i, rating, plus)->
        if i > -1 && text_rating[i] != rating[i]

            num = rating[i]
            e = $(".#{a.id}_#{i}")

            if(e.length == 0)
                $timeout(
                  ->
                    $scope.animate(a, text_rating, i, rating, plus)
                , 500)

            else
                e.animate({ top: -22 * plus }, 250, 'easeInCubic', ->
                    e.text(num);
                    e.css({ top: 22 * plus  });
                    $scope.animate(a, text_rating, i - 1, rating, plus)
                    e.animate({ top: 0 }, 250, 'easeOutCubic', ->);
                )
        else
            $timeout(
              ->
                a.rating = rating
             , 500)

    $scope.plus = (a, answers)->
        rating = (parseInt(a.rating) + 1).toString()
        text_rating = (rating.length - a.rating.length > 0 ? ' ' : '')+ a.rating
        for i in answers
            if answers[i].author == a.author
                answers[i].rating = text_rating;
                $scope.safeApply(->
                    $scope.animate(answers[i], text_rating, rating.length - 1, rating, 1)
                )


    $scope.minus = (a, answers)->
        rating = (parseInt(a.rating) - 1).toString();
        text_rating = a.rating;
        if a.rating.length != rating.length
            rating = "#{rating} "

        for i in answers
            if answers[i].author == a.author
                answers[i].rating = text_rating;
                $scope.safeApply(->
                    $scope.animate(answers[i], a.rating, rating.length - 1, rating, -1)
                )


)


.controller('ProfileFormCtrl',   ($routeParams, $resource, $q, $scope, $element, $http, $location, $rootScope) ->
   $.fields = {}
   $scope.submit = ->

      $http(
          method: 'POST'
          url: '/forum/profile/name/edit'
          data: $element.serialize()
          headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
      ).success((data, status)->
              fileds =
                    password: 'Пароль'
                    login: 'Логин'
                    email: 'E-mail'


              rules =
                    max_length: 'Максимальеая длина поля "%" 20 сиволов',
                    min_length: 'Минимальная длина поля "%" 5 сиволов',
                    required: '% слишком короткий',
                    unique: 'Такой % уже зарегистрирован',
                    valid_email: 'Неверный формат email'


              if data.id != undefined
                   success('Регистрация успешно завершена!', 3)
                   for d of data
                        $rootScope.user[d] = data[d]
                   location.path('/forum/')
              else
                   for f of data
                       error( rules[ data[f].rule].replace('%', fileds[f]) , 5)


      ).error( ->
                error('Нет соединения с сервером!', 5)
      )

  )

.controller('ProfileCtrl',   (City, User, $routeParams, $rootScope, $scope, $http, $location,$timeout)->
    $scope.profile = User.get($routeParams.username)

    datapicker = $('.datapicker').datepicker({'weekStart': 1})
      .on('changeDate', (ev) ->

      )


    $.ajax(
        url: '/assets/json/city_all.txt'
        dataType : 'json'
    ).done((data)->
      $rootScope.city = data
    );

    $.ajax(
        url: '/assets/json/country.txt'
        dataType : 'json'
    ).done((data)->
      $rootScope.country = data
    )

    $rootScope.jabber = ['jabber.ru', 'gmail.com', 'ya.ru', 'livejournal.com', 'vk.com', 'miranda.im',  'hiasm.com', 'vkontakte.ru', 'jabber.ua', 'jabber.kiev.ua', 'jabber.net.ua','jabber.org.ua', 'mytlt.ru', 'jabnet.org', 'jabber.org'];

)
.controller('AddUserFormCtrl',   ($routeParams, $resource, $q, $scope, $element, $http, $location, $rootScope) ->
    $scope.fields = {}

    $scope.submit = ->
        $http({
            method: 'POST'
            url: '/forum/registration'
            data: $element.serialize()
            headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success((data, status)->
            fileds = {
              password: 'Пароль'
              login: 'Логин'
              email: 'E-mail'
            }

            rules = {
                max_length: 'Максимальеая длина поля "%" 20 сиволов',
                min_length: 'Минимальная длина поля "%" 5 сиволов',
                required: '% слишком короткий',
                unique: 'Такой % уже зарегистрирован',
                valid_email: 'Неверный формат email'
            }

            if data.id != undefined
                success('Регистрация успешно завершена!', 3)
                for d of data
                    $rootScope.user[d] = data[d]
                $location.path('/forum/')
            else
                for f of data
                    error( rules[ data[f].rule].replace('%', fileds[f]) , 5)


        ).error(->
            error('Нет соединения с сервером!', 5)
        )

)
.controller('AuthCtrl', ($rootScope, $scope, $element, $http, $location) ->

    $scope.enable = true
    $scope.submit = ->
        $scope.enable = false
        $http({
            method: 'POST'
            url: '/forum/auth'
            data: $element.serialize()
            headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success((data, status)->

            if data

                $rootScope.safeApply(->
                    $rootScope.user = data
                )

                #$.cookie("#{id}_state", true, { expires: 9999})

                $('#auth').modal('hide')
                success('Авторизация успешна!', 3)
            else
                error('Не верный логин или пароль!', 3)

        ).error(->
            error('Нет соединения с сервером!', 5)
        )

)
.controller('AddThreadPageCtrl',  (Sections, $scope, $http, $location)->
    bind_editor()
    $scope.sections = Sections.get()
)
  .controller('AddThreadFormCtrl',   (Threads, $routeParams, Sections, $resource, $q, $scope, $element, $http, $location, $rootScope) ->
        $scope.fields = {}
        $scope.child = (section)->
            return section.parent != 0

        $scope.root = (section)->
            return section.parent*1 == 0


        $scope.required = (form, name)->
            return form[name].$error.required ? 'error' : ''


        $scope.submit = ->
            $rootScope.section_id = $($element.context).find('select').val()

            $http({
                method: 'POST'
                url: '/forum/thread/add'
                data: $element.serialize()
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success((data, status)->
                if data != []
                    notice('Тема успешно добавлена!', 3)
                    $location.path("/forum/section/#{$rootScope.section_id}/thread/#{data.id}/page/1")
                else
                    error('Произошла неизвестная ошибка!', 3)

            ).error(->
                error('Нет соединения с сервером!', 5)
            )
)
  .controller('SectionsCtrl', (Sections, $rootScope, $scope, $location, $http)->
      $scope.sections = Sections.get(
        ->
          unblockUI()
      )
      $scope.min = {}

      if $scope.sections.length < 1
          blockUI();

      $scope.getState = (id)->
          return $.cookie("#{id}_state") != null;

      $scope.minimize = ($event)->
          id = $($event.target).closest('tr').data('id')
          state = $scope.getState(id);
          if state == true
              $.removeCookie("#{id}_state")
          else
              $.cookie("#{id}_state", true, { expires: 9999})
)
.controller('ThreadsCtrl', (Sections, Threads, $rootScope, $scope, $location, $http, $routeParams)->

    $scope.pagination = {
      cur: $routeParams.page || 1
      total: 1
      display: 5
    }
    if $scope.threads == undefined
        blockUI()

    Threads.get((data)->
        $scope.breadcrumb = [
            { label: 'Форум', url: '#!/'}
            { label: data.info.name, url: '#!/'}
        ]

        $rootScope.section_id = data.info.id
        $scope.pagination.total = data.pages
        $scope.threads = data.threads
        unblockUI()
    )

    $scope.$watch('pagination.cur',
        (page)->
            if $routeParams.id
                $location.path("/forum/section/#{$routeParams.id}/page/#{page}")
    )
).controller('LastCtrl', (Threads, $rootScope, $scope, $location, $http, $routeParams)->
      $scope.pagination = {
        cur: 1
        total: 1
        display: 5
      }

      $scope.breadcrumb = [
          { label: 'Форум', url: '#!/'}
          { label: 'Последние', url: '#!/'}
      ]

      if $scope.threads == undefined
          blockUI()

      Threads.getLast((data)->
          $scope.pagination.total = data.pages
          $scope.threads = data.threads
          unblockUI()
      )
)
  .controller('AnswersCtrl', (Thread, Threads, Sections, $rootScope, Answers, $scope, $location, $http, $routeParams)->
      $scope.pagination = {
        cur: $routeParams.page || 1
        total: 1
        display: 5
      }

      $scope.fields = {}
      $scope.sections = Sections.get(unblockUI)
      $scope.admin_mode = $.cookie('admin_mode')
      $scope.section_id = $routeParams.sid

      if($scope.sections.length < 1)
          blockUI()

      $scope.child = (section)->
          return section.parent != 0

      $scope.root = (section)->
          return section.parent == 0

      $scope.is_closed = (close) ->
          return close && ($rootScope.user.status == undefined || $rootScope.user.status < 2)

      $scope.edit_attach = ->
          $scope.info.attached = ($scope.info.attached == 0 ? 1 : 0)
          Thread.save($scope.info, 'attached')

      $scope.edit_closed = ->
          $scope.info.closed = ($scope.info.closed == 0 ? 1 : 0)
          Thread.save($scope.info, 'closed')


      $scope.move_thread = (Section_Id)->
          $scope.info.section_id = Section_Id
          Thread.save($scope.info, 'move')
          $location.path("/forum/section/#{Section_Id}/thread/#{$scope.info.id}/page/#{$routeParams.page}" )

      $scope.name_save = (name)->
          $scope.info.name = name
          $scope.breadcrumb[2].label = name
          $scope.edit_name_show = false
          Thread.save($scope.info, 'name')

      $scope.thread_delete = ->
          if confirm("Действительно удалить?")
              $scope.info.section_id = 1
              Thread.save($scope.info, 'del')
              $location.path("/forum/section/#{$routeParams.sid}/")

      $scope.edit = (answer)->
          editor.answer_id = answer.id
          editor.setValue(answer.message)
          editor.focus()

      $scope.$watch('admin_mode', (mode)->
          $.cookie('admin_mode', mode)
      )

      $scope.$watch('pagination.cur', (page)->
          if $routeParams.sid != undefined
              $location.path("/forum/section/#{$routeParams.sid}/thread/#{$routeParams.tid}/page/#{page}")
      )

      bind_editor();

      $scope.$on('reloadAnswers', ->
          Answers.get((data)->
              $scope.pagination.total = data.pages
              $scope.answers = data.answers
              $scope.info = data.info
              $scope.breadcrumb = [
                  { label: 'Форум', url: '#!/'}
                  { label: data.info.section_name, url: "#!/forum/section/#{data.info.section_id}/page/1"}
                  { label: data.info.name,   url: ''}
              ]
          )
      )

      $scope.quote = (i)->
          answer = $scope.answers[i]
          username = answer.username || 'Гость'
          if getSelection() != ''
              selection = getSelection().toString().replace(/\n/, '<br/>')
          else
              selection = answer.message.replace(/<[/]?span>/gmi, '')


          editor.composer.focus();
          editor.composer.commands.exec("insertHTML",
              "<div class=\"alert\">" +
              "<strong>Цитата пользователя:</strong> #{username}"
              "<div> \"#{selection}\" </div>" +
              "</div>"
          )

      $scope.$emit('reloadAnswers');
)
  .controller('AddAnswerFormCtrl', ($routeParams, $scope, $element, $http, $location)->
      $scope.thread_id = $routeParams.tid

      $scope.submit = ->
          anser_id = editor.answer_id || 0

          $http({
              method: 'POST',
              url: "/forum/answer/add?answer_id=#{anser_id}"
              data: $element.serialize()
              headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
          }).success((data, status)->
              if data == '"permissions"'
                  error('Не достаточно прав для изменения сообщения!', 5)
              else
                  if data == '"ok"'
                      success('Сообщение успешно изменено!', 5)
                  editor.answer_id = 0
                  editor.setValue('')
                  $scope.$emit('reloadAnswers')

          ).error(->
              error('Нет соединения с сервером!', 5)
          )
)


@DevCtrl = ($scope) ->
  $scope.types = {
    fixed: 'Исправлено'
    added: 'Добавлено'
    code: 'Код'
    time: ''
  }

  $scope.cls = {
    fixed: 'info'
    added: 'success'
    code: 'warning'
    time: ''
  }

  $scope.log = [
    {type: 'time', message: '21.02.2013'},
    {type: 'fixed', message: 'Пофикшено неверно отображение дизайна в опере и лисе.'},
    {type: 'added', message: 'Улучшено отображение форума на экранах с низким разрешением.'},
    {type: 'time', message: '21.02.2013'},
    {type: 'added', message: 'Пофикшено неверно отображение дизайна в опере и лисе.'},
    {type: 'time', message: '17.02.2013'},
    {type: 'added', message: 'Добавлен datapicker для удобного ввода дат.'},
    {type: 'fixed', message: 'После авторизации логин появлялся только полсе перезагрузки страницы.'},
    {type: 'time', message: '15.02.2013'},
    {type: 'added', message: 'Автодополнение полей: Город(RU,UA,USA), Страна, jabber сервер.'},
    {type: 'time', message: '10.02.2013'},
    {type: 'added', message: 'Добавлена надпись с прросьбой подождать при загрузке  некоторых страниц.'},
    {type: 'time', message: '07.02.2013'},
    {type: 'added', message: 'Просмотр профиля.'},
    {type: 'time', message: '01.02.2013'},
    {type: 'added', message: 'Добавлена красивая анимация изменения рейтинга польщователя.'},
    {type: 'added', message: 'Изменён CSS цитат.'},
    {type: 'time', message: '30.01.2013'},
    {type: 'code', message: 'Проведено трестирование фильтра html редактора, -1 xss.'},
    {type: 'time', message: '27.01.2013'},
    {type: 'added', message: 'Кнопка изменить'},
    {type: 'time', message: '25.01.2013'},
    {type: 'fixed', message: 'Поправлено несколько тем(оставлльные темы будут поправлены после создания профиля), установлена ка главная тема white.'},
    {type: 'time', message: '24.01.2013'},
    {type: 'added', message: 'Возможность удаления, переименования, открытия, прикрепления, закрытия тем'},
    {type: 'time', message: '23.01.2013'},
    {type: 'added', message: 'Кнопки управления темами, сообщениями, рейтингом'},
    {type: 'time', message: '19.01.2013'},
    {type: 'fixed', message: 'Breadcrumb'},
    {type: 'time', message: '18.01.2013'},
    {type: 'added', message: 'Возможность прикрепление и закрытия тем'},
    {type: 'time', message: '15.01.2013'},
    {type: 'code', message: 'Переписана часть кода'},
    {type: 'fixed', message: 'Неверный перенос панели навигации при изменении разрешения меьше 800px'},
    {type: 'time', message: '13.01.2013'},
    {type: 'added', message: 'Регистрация'},
    {type: 'time', message: '12.01.2013'},
    {type: 'added', message: 'Добавлено ещё несколько тем оформления'},
    {type: 'time', message: '11.01.2013'},
    {type: 'added', message: 'Темы оформления'},
    {type: 'time', message: '10.01.2013'},
    {type: 'added', message: 'Небольшие изменения интерфейса'},
    {type: 'time', message: '9.01.2013'},
    {type: 'added', message: 'Локализация даты и времени'},
    {type: 'time', message: '8.01.2013'},
    {type: 'fixed', message: 'Теперь при выделении цитат копируется только текст'},
    {type: 'added', message: 'Кэширование запросов (при возврате назад, данные не грузятся)'}
  ]

  for i of $scope.log
      $scope.log[i].label = $scope.types[$scope.log[i].type]

  $scope.class = (type)->
    return "label label-#{$scope.cls[type]}"


  $scope.cols = (d)->
    return d ? 1 : 2


@themeSwitcherCtrl = ($scope)->
    $scope.themes = []
    $scope.setTheme = (theme)->
        $('#theme_css').attr('href', "/assets/css/themes/bootstrap.#{theme}.min.css.gz")

@info_show = (message, type = 'error', time)->
    headers = {error: 'Ошибка', notice: 'Уведомление', warning: 'Предупреждение', success: 'Успешно'}
    header = headers[type]
    alert = $('div#alert')

    alert.attr('class', "alert alert-{#type} fade in").show().find('span').html("<strong>#{header}!</strong> #{message}")
    alert.show();
    if time != undefined
      setTimeout(
          ->
            alert.fadeOut()
          , time * 1000)


@notice = (message, time)->
    info_show(message, 'notice', time)

@error = (message, time)->
    info_show(message, 'error', time)

@warning = (message, time)->
    info_show(message, 'warning', time)

@success = (message, time)->
    info_show(message, 'success', time)

# bind wysihtml5 editor

@bind_editor =
  ->
    @editor = new wysihtml5.Editor("wysihtml5-editor",
        {
            toolbar:     "wysihtml5-editor-toolbar",
            stylesheets: ["/assets/css/editor.css", "/assets/css/bootstrap.min.css.gz"],
            parserRules: wysihtml5ParserRules,
            locale: "ru-RU"
        }
    )

    wysihtml5.commands.table =
        {
          exec: (composer, command, param)->
             elem = $('<table class="table table-bordered">' +
                "\r\n                    <thead>"+
                "\r\n                        <tr>"+
                "\r\n                            <th>#</th>"+
                "\r\n                            <th></th>"+
                "\r\n                            <th></th>"+
                "\r\n                            <th></th>"+
                "\r\n                        </tr>"+
                "\r\n                    </thead>"+
                "\r\n                    <tbody>"+
                "\r\n                        <tr>"+
                "\r\n                            <td>1</td>"+
                "\r\n                            <td></td>"+
                "\r\n                            <td></td>"+
                "\r\n                           <td></td>"+
                "\r\n                       </tr>"+
                "\r\n                       <tr>"+
                "\r\n                           <td>2</td>"+
                "\r\n                           <td></td>"+
                "\r\n                           <td></td>"+
                "\r\n                            <td></td>"+
                "\r\n                       </tr>"+
                "\r\n                       <tr>"+
                "\r\n                          <td>3</td>"+
                "\r\n                           <td></td>"+
                "\r\n                           <td></td>"+
                "\r\n                           <td></td>"+
                "\r\n                       </tr>"+
                "\r\n                   </tbody>"+
                "\r\n               </table>")
             composer.selection.insertNode(elem[0])

          state: (composer, command)->
              return wysihtml5.commands.formatInline.state(composer, command, "table")

          value:
            ->
        }

#@getSelection()->
#    return (!!document.getSelection) ? document.getSelection() :
#    (!!window.getSelection) ? window.getSelection() :
#    document.selection.createRange().text


angular.bootstrap document, ["app"]