// Generated by CoffeeScript 1.6.1
(function() {

  this.app = angular.module('app', ['ngResource'], function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('!').html5Mode(false);
    $routeProvider.when('/services', {
      templateUrl: 'services.html',
      controller: 'SectionsCtrl'
    }).when('/forum/section/:id/page/:page', {
      templateUrl: 'threads.html',
      controller: 'ThreadsCtrl'
    }).when('/forum/thread/new', {
      templateUrl: 'newThread.html',
      controller: 'AddThreadPageCtrl'
    }).when('/forum/section/:sid/thread/:tid/page/:page', {
      templateUrl: 'answers.html',
      controller: 'AnswersCtrl'
    }).when('/forum/last', {
      templateUrl: 'threads.html',
      controller: 'LastCtrl'
    }).when('/forum', {
      templateUrl: 'sections.html',
      controller: 'SectionsCtrl'
    }).when('/dev', {
      templateUrl: 'dev.html',
      controller: 'DevCtrl'
    }).when('/forum/registration', {
      templateUrl: 'reg.html',
      controller: 'UserCtrl'
    }).when('/forum/sha', {
      templateUrl: 'sha.html',
      controller: 'UserCtrl'
    }).when('/forum/profile/:username', {
      templateUrl: 'profile.html',
      controller: 'ProfileCtrl'
    }).when('/forum/profile/:username/edit', {
      templateUrl: 'profile_edit.html',
      controller: 'ProfileCtrl'
    });
    return $routeProvider.otherwise({
      redirectTo: '/forum'
    });
  }).directive('uiBreadcrumb', function() {
    return {
      restrict: 'E',
      template: "<ul class=\"breadcrumb\" style=\"margin: 0px\"><li ng-repeat='node in breadcrumb' ng-class=\"{active: $last}\" ><span ng-show=\"$last\">{{node.label}}</span><a ng-hide=\"$last\" ng-href='{{node.url}}'><i ng-show=\"$first\" class=\"icon icon-home\"></i> {{node.label}}</a> <span ng-hide=\"$last\" class=\"divider\">/</span></li></ul>",
      replace: true
    };
  }).directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs, City) {
      var autocomplete;
      autocomplete = iElement.typeahead();
      return scope.$watch(iAttrs.uiItems, function(value) {
        return autocomplete.data('typeahead').source = value;
      }, true);
    };
  }).directive('uiPagination', function() {
    var object;
    return object = {
      restrict: 'EA',
      replace: true,
      template: '<div class="pagination pagination-centered" style="margin: 0px;height: 29px">' + '<ul>' + '<li ng-class="{disabled: firstPage()}" ng-click="goToFirstPage()">' + '<a><i class="icon-step-backward"></i></a>' + '</li>' + '<li ng-class="{disabled: !hasPrev()}" ng-click="prev()">' + '<a><i class="icon-chevron-left"></i></a>' + '</li>' + '<li ng-repeat="page in pages"' + 'ng-class="{active: isCurrent(page)}"' + 'ng-click="setCurrent(page)"' + '>' + '<a>{{page}}</a>' + '</li>' + '<li ng-class="{disabled: !hasNext()}" ng-click="next()">' + '<a><i class="icon-chevron-right"></i></a>' + '</li>' + '<li ng-class="{disabled: lastPage()}" ng-click="goToLastPage()">' + '<a><i class="icon-step-forward"></i></a>' + '</li>' + '</ul>' + '</div>',
      scope: {
        cur: '=',
        total: '=',
        display: '@'
      },
      link: function(scope, element, attrs) {
        var calcPages;
        calcPages = function() {
          var delta, display, end, start, _i, _results;
          display = +scope.display;
          delta = Math.floor(display / 2);
          scope.start = scope.cur - delta;
          if (scope.start < 1) {
            scope.start = 1;
          }
          scope.end = scope.start + display - 1;
          if (scope.end > scope.total) {
            scope.end = scope.total;
            scope.start = scope.end - (display - 1);
            if (scope.start < 1) {
              scope.start = 1;
            }
          }
          start = scope.start;
          end = scope.end;
          return scope.pages = (function() {
            _results = [];
            for (var _i = start; start <= end ? _i <= end : _i >= end; start <= end ? _i++ : _i--){ _results.push(_i); }
            return _results;
          }).apply(this);
        };
        scope.$watch('cur', calcPages);
        scope.$watch('total', calcPages);
        scope.$watch('display', calcPages);
        scope.isCurrent = function(index) {
          return scope.cur === index;
        };
        scope.setCurrent = function(index) {
          return scope.cur = index;
        };
        scope.hasPrev = function() {
          return scope.cur > 1;
        };
        scope.prev = function() {
          if (scope.hasPrev()) {
            return scope.cur--;
          }
        };
        scope.hasNext = function() {
          return scope.cur < scope.total;
        };
        scope.next = function() {
          if (scope.hasNext()) {
            return scope.cur++;
          }
        };
        scope.firstPage = function() {
          return scope.start === 1;
        };
        scope.goToFirstPage = function() {
          if (!scope.firstPage()) {
            return scope.cur = 1;
          }
        };
        scope.lastPage = function() {
          return scope.end === scope.total;
        };
        return scope.goToLastPage = function() {
          if (!scope.lastPage()) {
            return scope.cur = scope.total;
          }
        };
      }
    };
  }).factory('Sections', function($rootScope, $http, $q, $routeParams, $timeout, $resource, $cacheFactory) {
    var Sections, cache;
    cache = $cacheFactory('cacheSections');
    Sections = $resource('/forum/sections/', {}, {
      get: {
        method: 'GET',
        isArray: true
      }
    });
    return {
      get: function(callback) {
        var sections;
        sections = cache.get(1);
        if (!sections) {
          sections = Sections.get(callback);
          cache.put(1, sections);
        } else {
          if (callback) {
            callback(sections);
          }
        }
        return sections;
      }
    };
  }).factory('User', function($rootScope, $http, $q, $routeParams, $timeout, $resource, $cacheFactory) {
    var User, cache;
    cache = $cacheFactory('cacheUser');
    User = $resource('/forum/user/:username', $routeParams, {
      get: {
        method: 'GET',
        isArray: false
      }
    });
    return {
      get: function(username) {
        var user;
        username = username || $routeParams.username;
        user = cache.get(username);
        if (!user) {
          user = User.get({
            username: username
          });
          cache.put(username, user);
        }
        return user;
      }
    };
  }).factory('City', function($http, $q, $timeout, $resource, $cacheFactory) {
    var cache_city;
    cache_city = $cacheFactory('cacheCity');
    return {
      get: function(callback) {
        var City, city_ru;
        City = $resource('/assets/json/city_ru.txt', {}, {
          get: {
            isArray: true,
            method: 'GET'
          }
        });
        city_ru = cache_city.get('city_ru');
        if (!city_ru) {
          city_ru = City.get(callback);
          cache_city.put('city_ru', city_ru);
        }
        if (callback) {
          callback(cache_city);
        }
        return city_ru;
      }
    };
  }).factory('Threads', function($routeParams, $http, $q, $timeout, $resource, $cacheFactory) {
    var cache;
    cache = $cacheFactory('cacheSection');
    return {
      get: function(callback) {
        var Threads, section;
        Threads = $resource('/forum/section/:id/page/:page', $routeParams);
        section = Threads.get(callback);
        return section;
      },
      getLast: function(callback) {
        var ThreadsLast;
        ThreadsLast = $resource('/forum/last');
        return ThreadsLast.get(callback);
      }
    };
  }).factory('Thread', function(Threads, $rootScope, $routeParams, $http, $q, $timeout, $resource, $cacheFactory) {
    return {
      save: function(p, t) {
        var i, _i, _len, _ref, _ref1, _ref2;
        if ($rootScope.user.status > 1) {
          if (t === 'attached') {
            success('Тема успешно ' + ((_ref = !p.attached) != null ? _ref : {
              'от': 'при'
            })(+'креплена', 3));
          }
          if (t === 'closed') {
            success('Тема успешно ' + ((_ref1 = !p.closed) != null ? _ref1 : {
              'от': 'за'
            })(+'крыта', 3));
          }
          if (t === 'name') {
            success('Тема успешно переименована', 3);
          }
          if (t === 'move') {
            success('Тема успешно перемещена', 3);
          }
          if (t === 'del') {
            success('Тема успешно удалена', 3);
          }
        } else {
          error('Доступ запрещён!', 3);
        }
        for (_i = 0, _len = p.length; _i < _len; _i++) {
          i = p[_i];
          p[i] = (_ref2 = p[i] === null) != null ? _ref2 : {
            '': p[i]
          };
        }
        return $resource('/forum/thread/:id/page/1', p, {
          save: {
            method: 'PUT'
          }
        }).save();
      }
    };
  }).run(function(Threads, $q, $routeParams, $rootScope, Sections, $http, $resource) {
    $rootScope.activeTab = '';
    $rootScope.user = CONFIG.user;
    $rootScope.$on('$routeChangeStart', function($scope, next) {
      return $scope.section_id = next.params.id || '' + next.params.sid || '';
    });
    $rootScope.rating = function(rating, i) {
      if (rating === 'start') {
        return [1, 2, 3, 4, 5];
      }
      if (i < rating) {
        return 'star__on.png';
      } else {
        return 'star__off.png';
      }
    };
    return $rootScope.safeApply = function(fn) {
      var phase;
      phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof fn === 'function')) {
          return fn();
        }
      } else {
        return this.$apply(fn);
      }
    };
  }).controller('NavCtrl', function($rootScope, $scope, $location, $routeParams) {
    $scope.subColors = ['DE662B', '21CFDC', '1F901B', 'EDDE45', '5882B8'];
    $scope.navClass = function(page, menu, key) {
      var _class;
      page = page.substring(2);
      if ($location.path() === page || $location.path().indexOf(page.substring(2)) !== -1) {
        _class = 'active';
        $rootScope.activeTab = key;
      } else {
        _class = '';
      }
      return _class;
    };
    $scope.getSubMenu = function() {
      if ($scope.items[$rootScope.activeTab]) {
        return $scope.items[$rootScope.activeTab].menu;
      }
    };
    $scope.isThread = function() {
      return $routeParams.tid;
    };
    return $scope.items = [
      {
        name: 'Форум',
        href: '#!/forum',
        menu: [
          {
            name: 'Последние',
            'href': '#!/forum/last'
          }, {
            name: 'Лог разработки',
            'href': '#!/dev'
          }, {
            name: 'Прочесть все',
            'href': '#!/forum'
          }, {
            name: 'Новая тема',
            'href': '#!/forum/thread/new'
          }
        ]
      }, {
        name: 'Сервисы',
        href: '#!/services',
        menu: []
      }, {
        name: 'Пользователи',
        href: '#!/services',
        menu: [
          {
            name: 'Оценки',
            'href': '#!/forum'
          }, {
            name: 'Баны',
            'href': '#!/forum'
          }, {
            name: 'Статистика',
            'href': '#!/forum'
          }
        ]
      }
    ];
  }).factory('Answers', function($routeParams, $http, $q, $timeout, $resource) {
    return $resource('/forum/thread/:tid/page/:page', $routeParams);
  }).controller('UserCtrl', function($rootScope, $scope, $http, $location, $timeout) {
    $scope.exit = function() {
      $.removeCookie('token', {
        path: '/forum'
      });
      return $rootScope.user = {};
    };
    $scope.fields = {};
    $scope.animate = function(a, text_rating, i, rating, plus) {
      var e, num;
      if (i > -1 && text_rating[i] !== rating[i]) {
        num = rating[i];
        e = $('.' + a.id + '_' + i);
        if (e.length === 0) {
          return $timeout(function() {
            return $scope.animate(a, text_rating, i, rating, plus);
          }, 500);
        } else {
          return e.animate({
            top: -22 * plus
          }, 250, 'easeInCubic', function() {
            e.text(num);
            e.css({
              top: 22 * plus
            });
            $scope.animate(a, text_rating, i - 1, rating, plus);
            return e.animate({
              top: 0
            }, 250, 'easeOutCubic', function() {});
          });
        }
      } else {
        return $timeout(function() {
          return a.rating = rating;
        }, 500);
      }
    };
    $scope.plus = function(a, answers) {
      var i, rating, text_rating, _i, _len, _ref, _results;
      rating = (parseInt(a.rating) + 1).toString();
      text_rating = ((_ref = rating.length - a.rating.length > 0) != null ? _ref : {
        ' ': ''
      }) + a.rating;
      _results = [];
      for (_i = 0, _len = answers.length; _i < _len; _i++) {
        i = answers[_i];
        if (answers[i].author === a.author) {
          answers[i].rating = text_rating;
          _results.push($scope.safeApply(function() {
            return $scope.animate(answers[i], text_rating, rating.length - 1, rating, 1);
          }));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    return $scope.minus = function(a, answers) {
      var i, rating, text_rating, _i, _len, _results;
      rating = (parseInt(a.rating) - 1).toString();
      text_rating = a.rating;
      if (a.rating.length !== rating.length) {
        rating = rating + ' ';
      }
      _results = [];
      for (_i = 0, _len = answers.length; _i < _len; _i++) {
        i = answers[_i];
        if (answers[i].author === a.author) {
          answers[i].rating = text_rating;
          _results.push($scope.safeApply(function() {
            return $scope.animate(answers[i], a.rating, rating.length - 1, rating, -1);
          }));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
  }).controller('ProfileCtrl', function(City, User, $routeParams, $rootScope, $scope, $http, $location, $timeout) {
    var datapicker;
    $scope.profile = User.get($routeParams.username);
    datapicker = $('.datapicker').datepicker({
      'weekStart': 1
    }).on('changeDate', function(ev) {});
    $.ajax({
      url: '/assets/json/city_all.txt',
      dataType: 'json'
    }).done(function(data) {
      return $scope.city = data;
    });
    $.ajax({
      url: '/assets/json/country.txt',
      dataType: 'json'
    }).done(function(data) {
      return $scope.country = data;
    });
    $scope.jabber = ['jabber.ru', 'gmail.com', 'ya.ru', 'livejournal.com', 'vk.com', 'miranda.im', 'hiasm.com', 'vkontakte.ru', 'jabber.ua', 'jabber.kiev.ua', 'jabber.net.ua', 'jabber.org.ua', 'mytlt.ru', 'jabnet.org', 'jabber.org'];
    return $scope.getSelected = function(term, type) {
      var i, ret, str, _i, _j, _ref, _ref1;
      if (!term) {
        term = '';
      }
      if (type === 'jabber') {
        if (term.indexOf('@') === -1) {
          return false;
        } else {
          if (term.indexOf('@') === term.length(-1)) {
            ret = [];
            for (i = _i = 0, _ref = $scope[type].length; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
              ret[i] = term + $scope[type][i];
            }
            return ret;
          } else {
            ret = [];
            str = term.substr(term.indexOf('@') + 1);
            for (i = _j = 0, _ref1 = $scope[type].length; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; i = 0 <= _ref1 ? ++_j : --_j) {
              if ($scope[type][i].substr(0, str.length) === str) {
                ret.push(term + $scope[type][i].substr(str.length));
              }
            }
            return ret;
          }
        }
      }
      return $scope[type];
    };
  }).controller('AddUserFormCtrl', function($routeParams, $resource, $q, $scope, $element, $http, $location, $rootScope) {
    $scope.fields = {};
    return $scope.submit = function() {
      return $http({
        method: 'POST',
        url: '/forum/registration',
        data: $element.serialize(),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).success(function(data, status) {
        var d, f, fileds, rules, _results;
        fileds = {
          password: 'Пароль',
          login: 'Логин',
          email: 'E-mail'
        };
        rules = {
          max_length: 'Максимальеая длина поля "%" 20 сиволов',
          min_length: 'Минимальная длина поля "%" 5 сиволов',
          required: '% слишком короткий',
          unique: 'Такой % уже зарегистрирован',
          valid_email: 'Неверный формат email'
        };
        if (data.id !== void 0) {
          success('Регистрация успешно завершена!', 3);
          for (d in data) {
            $rootScope.user[d] = data[d];
          }
          return $location.path('/forum/');
        } else {
          _results = [];
          for (f in data) {
            _results.push(error(rules[data[f].rule].replace('%', fileds[f]), 5));
          }
          return _results;
        }
      }).error(function() {
        return error('Нет соединения с сервером!', 5);
      });
    };
  }).controller('AuthCtrl', function($rootScope, $scope, $element, $http, $location) {
    $scope.enable = true;
    return $scope.submit = function() {
      $scope.enable = false;
      return $http({
        method: 'POST',
        url: '/forum/auth',
        data: $element.serialize(),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).success(function(data, status) {
        if (data) {
          $rootScope.safeApply(function() {
            return $rootScope.user = data;
          });
          $('#auth').modal('hide');
          return success('Авторизация успешна!', 3);
        } else {
          return error('Не верный логин или пароль!', 3);
        }
      }).error(function() {
        return error('Нет соединения с сервером!', 5);
      });
    };
  }).controller('AddThreadPageCtrl', function(Sections, $scope, $http, $location) {
    bind_editor();
    return $scope.sections = Sections.get();
  }).controller('AddThreadFormCtrl', function(Threads, $routeParams, Sections, $resource, $q, $scope, $element, $http, $location, $rootScope) {
    $scope.fields = {};
    $scope.child = function(section) {
      return section.parent !== 0;
    };
    $scope.root = function(section) {
      return section.parent === 0;
    };
    $scope.required = function(form, name) {
      var _ref;
      return (_ref = form[name].$error.required) != null ? _ref : {
        'error': ''
      };
    };
    return $scope.submit = function() {
      $rootScope.section_id = $($element.context).find('select').val();
      return $http({
        method: 'POST',
        url: '/forum/thread/add',
        data: $element.serialize(),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).success(function(data, status) {
        if (data !== []) {
          notice('Тема успешно добавлена!', 3);
          return $location.path('/forum/section/' + $rootScope.section_id(+'/thread/' + data.id + '/page/1'));
        } else {
          return error('Произошла неизвестная ошибка!', 3);
        }
      }).error(function() {
        return error('Нет соединения с сервером!', 5);
      });
    };
  }).controller('SectionsCtrl', function(Sections, $rootScope, $scope, $location, $http) {
    $scope.sections = Sections.get(function() {
      return unblockUI();
    });
    $scope.min = {};
    if ($scope.sections.length < 1) {
      blockUI();
    }
    $scope.getState = function(id) {
      return $.cookie(id + '_state') !== null;
    };
    return $scope.minimize = function($event) {
      var id, state;
      id = $($event.target).closest('tr').data('id');
      state = $scope.getState(id);
      if (state === true) {
        return $.removeCookie(id + '_state');
      } else {
        return $.cookie(id + '_state', true, {
          expires: 9999
        });
      }
    };
  }).controller('ThreadsCtrl', function(Sections, Threads, $rootScope, $scope, $location, $http, $routeParams) {
    $scope.pagination = {
      cur: $routeParams.page || 1,
      total: 1,
      display: 5
    };
    if ($scope.threads === void 0) {
      blockUI();
    }
    Threads.get(function(data) {
      $scope.breadcrumb = [
        {
          label: 'Форум',
          url: '#!/'
        }, {
          label: data.info.name,
          url: '#!/'
        }
      ];
      $rootScope.section_id = data.info.id;
      $scope.pagination.total = data.pages;
      $scope.threads = data.threads;
      return unblockUI();
    });
    return $scope.$watch('pagination.cur', function(page) {
      if ($routeParams.id) {
        return $location.path("/forum/section/" + $routeParams.id + "/page/" + page);
      }
    });
  }).controller('LastCtrl', function(Threads, $rootScope, $scope, $location, $http, $routeParams) {
    $scope.pagination = {
      cur: 1,
      total: 1,
      display: 5
    };
    $scope.breadcrumb = [
      {
        label: 'Форум',
        url: '#!/'
      }, {
        label: 'Последние',
        url: '#!/'
      }
    ];
    if ($scope.threads === void 0) {
      blockUI();
    }
    return Threads.getLast(function(data) {
      $scope.pagination.total = data.pages;
      $scope.threads = data.threads;
      return unblockUI();
    });
  }).controller('AnswersCtrl', function(Thread, Threads, Sections, $rootScope, Answers, $scope, $location, $http, $routeParams) {
    $scope.pagination = {
      cur: $routeParams.page || 1,
      total: 1,
      display: 5
    };
    $scope.fields = {};
    $scope.sections = Sections.get(unblockUI);
    $scope.admin_mode = $.cookie('admin_mode');
    $scope.section_id = $routeParams.sid;
    if ($scope.sections.length < 1) {
      blockUI();
    }
    $scope.child = function(section) {
      return section.parent !== 0;
    };
    $scope.root = function(section) {
      return section.parent === 0;
    };
    $scope.is_closed = function(close) {
      return close && ($rootScope.user.status === void 0 || $rootScope.user.status < 2);
    };
    $scope.edit_attach = function() {
      var _ref;
      $scope.info.attached = (_ref = $scope.info.attached === 0) != null ? _ref : {
        1: 0
      };
      return Thread.save($scope.info, 'attached');
    };
    $scope.edit_closed = function() {
      var _ref;
      $scope.info.closed = (_ref = $scope.info.closed === 0) != null ? _ref : {
        1: 0
      };
      return Thread.save($scope.info, 'closed');
    };
    $scope.move_thread = function(Section_Id) {
      $scope.info.section_id = Section_Id;
      Thread.save($scope.info, 'move');
      return $location.path('/forum/section/' + Section_Id(+'/thread/' + $scope.info.id + '/page/' + $routeParams.page));
    };
    $scope.name_save = function(name) {
      $scope.info.name = name;
      $scope.breadcrumb[2].label = name;
      $scope.edit_name_show = false;
      return Thread.save($scope.info, 'name');
    };
    $scope.thread_delete = function() {
      if (confirm("Действительно удалить?")) {
        $scope.info.section_id = 1;
        Thread.save($scope.info, 'del');
        return $location.path('/forum/section/' + $routeParams.sid(+'/'));
      }
    };
    $scope.edit = function(answer) {
      editor.answer_id = answer.id;
      editor.setValue(answer.message);
      return editor.focus();
    };
    $scope.$watch('admin_mode', function(mode) {
      return $.cookie('admin_mode', mode);
    });
    $scope.$watch('pagination.cur', function(page) {
      if ($routeParams.sid !== void 0) {
        return $location.path('/forum/section/' + $routeParams.sid(+'/thread/' + $routeParams.tid(+'/page/' + page)));
      }
    });
    bind_editor();
    $scope.$on('reloadAnswers', function() {
      return Answers.get(function(data) {
        $scope.pagination.total = data.pages;
        $scope.answers = data.answers;
        $scope.info = data.info;
        return $scope.breadcrumb = [
          {
            label: 'Форум',
            url: '#!/'
          }, {
            label: data.info.section_name,
            url: '#!/forum/section/' + data.info.section_id + '/page/1'
          }, {
            label: data.info.name,
            url: ''
          }
        ];
      });
    });
    $scope.quote = function(i) {
      var answer, selection, username;
      answer = $scope.answers[i];
      username = answer.username || 'Гость';
      if (getSelection() !== '') {
        selection = getSelection().toString().replace(/\n/, '<br/>');
      } else {
        selection = answer.message.replace(/<[/]?span>/gmi, '');
      }
      editor.composer.focus();
      return editor.composer.commands.exec("insertHTML", "<div class=\"alert\">" + "<strong>Цитата пользователя:</strong> " + username + "<div> \"" + selection(+"\" </div>" + "</div>"));
    };
    return $scope.$emit('reloadAnswers');
  }).controller('AddAnswerFormCtrl', function($routeParams, $scope, $element, $http, $location) {
    $scope.thread_id = $routeParams.tid;
    return $scope.submit = function() {
      var anser_id;
      anser_id = editor.answer_id || 0;
      return $http({
        method: 'POST',
        url: '/forum/answer/add?answer_id=' + anser_id,
        data: $element.serialize(),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).success(function(data, status) {
        if (data === '"permissions"') {
          return error('Не достаточно прав для изменения сообщения!', 5);
        } else {
          if (data === '"ok"') {
            success('Сообщение успешно изменено!', 5);
          }
          editor.answer_id = 0;
          editor.setValue('');
          return $scope.$emit('reloadAnswers');
        }
      }).error(function() {
        return error('Нет соединения с сервером!', 5);
      });
    };
  });

  this.DevCtrl = function($scope) {
    var i;
    $scope.types = {
      fixed: 'Исправлено',
      added: 'Добавлено',
      code: 'Код',
      time: ''
    };
    $scope.cls = {
      fixed: 'info',
      added: 'success',
      code: 'warning',
      time: ''
    };
    $scope.log = [
      {
        type: 'time',
        message: '21.02.2013'
      }, {
        type: 'fixed',
        message: 'Пофикшено неверно отображение дизайна в опере и лисе.'
      }, {
        type: 'added',
        message: 'Улучшено отображение форума на экранах с низким разрешением.'
      }, {
        type: 'time',
        message: '21.02.2013'
      }, {
        type: 'added',
        message: 'Пофикшено неверно отображение дизайна в опере и лисе.'
      }, {
        type: 'time',
        message: '17.02.2013'
      }, {
        type: 'added',
        message: 'Добавлен datapicker для удобного ввода дат.'
      }, {
        type: 'fixed',
        message: 'После авторизации логин появлялся только полсе перезагрузки страницы.'
      }, {
        type: 'time',
        message: '15.02.2013'
      }, {
        type: 'added',
        message: 'Автодополнение полей: Город(RU,UA,USA), Страна, jabber сервер.'
      }, {
        type: 'time',
        message: '10.02.2013'
      }, {
        type: 'added',
        message: 'Добавлена надпись с прросьбой подождать при загрузке  некоторых страниц.'
      }, {
        type: 'time',
        message: '07.02.2013'
      }, {
        type: 'added',
        message: 'Просмотр профиля.'
      }, {
        type: 'time',
        message: '01.02.2013'
      }, {
        type: 'added',
        message: 'Добавлена красивая анимация изменения рейтинга польщователя.'
      }, {
        type: 'added',
        message: 'Изменён CSS цитат.'
      }, {
        type: 'time',
        message: '30.01.2013'
      }, {
        type: 'code',
        message: 'Проведено трестирование фильтра html редактора, -1 xss.'
      }, {
        type: 'time',
        message: '27.01.2013'
      }, {
        type: 'added',
        message: 'Кнопка изменить'
      }, {
        type: 'time',
        message: '25.01.2013'
      }, {
        type: 'fixed',
        message: 'Поправлено несколько тем(оставлльные темы будут поправлены после создания профиля), установлена ка главная тема white.'
      }, {
        type: 'time',
        message: '24.01.2013'
      }, {
        type: 'added',
        message: 'Возможность удаления, переименования, открытия, прикрепления, закрытия тем'
      }, {
        type: 'time',
        message: '23.01.2013'
      }, {
        type: 'added',
        message: 'Кнопки управления темами, сообщениями, рейтингом'
      }, {
        type: 'time',
        message: '19.01.2013'
      }, {
        type: 'fixed',
        message: 'Breadcrumb'
      }, {
        type: 'time',
        message: '18.01.2013'
      }, {
        type: 'added',
        message: 'Возможность прикрепление и закрытия тем'
      }, {
        type: 'time',
        message: '15.01.2013'
      }, {
        type: 'code',
        message: 'Переписана часть кода'
      }, {
        type: 'fixed',
        message: 'Неверный перенос панели навигации при изменении разрешения меьше 800px'
      }, {
        type: 'time',
        message: '13.01.2013'
      }, {
        type: 'added',
        message: 'Регистрация'
      }, {
        type: 'time',
        message: '12.01.2013'
      }, {
        type: 'added',
        message: 'Добавлено ещё несколько тем оформления'
      }, {
        type: 'time',
        message: '11.01.2013'
      }, {
        type: 'added',
        message: 'Темы оформления'
      }, {
        type: 'time',
        message: '10.01.2013'
      }, {
        type: 'added',
        message: 'Небольшие изменения интерфейса'
      }, {
        type: 'time',
        message: '9.01.2013'
      }, {
        type: 'added',
        message: 'Локализация даты и времени'
      }, {
        type: 'time',
        message: '8.01.2013'
      }, {
        type: 'fixed',
        message: 'Теперь при выделении цитат копируется только текст'
      }, {
        type: 'added',
        message: 'Кэширование запросов (при возврате назад, данные не грузятся)'
      }
    ];
    for (i in $scope.log) {
      $scope.log[i].label = $scope.types[$scope.log[i].type];
    }
    $scope["class"] = function(type) {
      return 'label label-' + $scope.cls[type];
    };
    return $scope.cols = function(d) {
      return d != null ? d : {
        1: 2
      };
    };
  };

  this.themeSwitcherCtrl = function($scope) {
    $scope.themes = [];
    return $scope.setTheme = function(theme) {
      return $('#theme_css').attr('href', '/assets/css/themes/bootstrap.' + theme + '.min.css.gz');
    };
  };

  angular.bootstrap(document, ["app"]);

}).call(this);
